/**
 * Created by kimduk on 01.06.15.
 */

$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'YYYY-DD-MM'
    });

    $(".fancybox").fancybox();

    $('.show-modal').bind('click', function(info) {
        var id = $(this).data('id');
        $.ajax({
            url : '/books/show/?id=' + id,
            type: "GET",
            success:function(data) {
                $('#show-modal .modal-content').html(data);
                $('#show-modal').modal('show');
            },
            error: function(jqXHR, textStatus, errorThrown) {
            }
        });

    });
});