<?php
use yii\helpers\Html;
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="show-modal_label">
        <?php echo $book->name; ?>
    </h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <?php echo Html::img($book->preview, ['class' => 'img-rounded',
                'height' => '300px']); ?>
        </div>
        <div class="col-md-6">
            <strong>Author: </strong> <?php echo $book->author->fullname; ?>
            <hr/>
            <strong>Release date: </strong> <?php echo date('d F Y', strtotime($book->date)); ?>
            <hr/>
            <strong>Created: </strong> <?php echo date('m/d/Y H:i', $book->date_create); ?>
            <hr/>
            <strong>Updated: </strong> <?php echo date('m/d/Y H:i', $book->date_update); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
