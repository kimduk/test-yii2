<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 03.06.15
 * Time: 10:25
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
$this->title = 'Books Yii2 Edit';
?>
<div class="body-content">
    <?php $form = ActiveForm::begin(['id' => 'edit-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-3">
            <?php echo $form->field($bookModel, 'author_id')
                ->dropDownList(array_merge(
                    [0 => 'Choose Author Name'], ArrayHelper::map($authors, 'id', 'fullname')
                ))->label(false); ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($bookModel, 'name',
                ['inputOptions' =>
                    ['placeholder' => 'Enter book name...']
                ])->label(false); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?php echo $form->field($bookModel, 'date',
                ['inputOptions' =>
                    ['placeholder' => 'Release date ...', 'class' => 'form-control datetimepicker']
                ])->label(false); ?>
        </div>
        <div class="col-md-3">
            <img src="/<?php echo $bookModel->preview; ?>" alt="" class="img-thumbnail"/>
            <?php echo $form->field($bookModel, 'imageFile')->fileInput()->label(false); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 pull-left">
            <br/>
            <?php echo Html::submitButton('Back',
                ['class' => 'btn btn-primary', 'name' => 'back-button', 'onclick' => 'javascript:history.back(1);']) ?>
        </div>
        <div class="col-md-3 pull-right">
            <br/>
            <?php echo Html::submitButton('Save',
                ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

