<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
$this->title = 'Books Yii2 Application';

?>
<div class="site-index">
    <?php $form = ActiveForm::begin(['id' => 'search-form']); ?>
        <div class="row">
            <div class="col-md-3">
                <?php echo $form->field($searchForm, 'authorId')
                    ->dropDownList(array_merge(
                        [0 => 'Choose Author Name'], ArrayHelper::map($authors, 'id', 'fullname')
                    ))->label(false); ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->field($searchForm, 'name',
                    ['inputOptions' =>
                        ['placeholder' => 'Enter book name...']
                    ])->label(false); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <strong>Release date: </strong>
                <?php echo $form->field($searchForm, 'releaseDateFrom',
                    ['inputOptions' =>
                        ['placeholder' => 'date from', 'class' => 'form-control datetimepicker']
                    ])->label(false); ?>
            </div>
            <div class="col-md-3">
                <br/>
                <?php echo $form->field($searchForm, 'releaseDateTo',
                    ['inputOptions' =>
                        ['placeholder' => 'date to', 'class' => 'form-control datetimepicker']
                    ])->label(false); ?>
            </div>
            <div class="col-md-3 pull-right">
                <br/>
                <?php echo Html::submitButton('Search',
                    ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    <div class="body-content">
        <table class="table table-striped table-hover">
            <tr>
                <td>
                    <?php
                    $alt = ($order === 'id' and $dir === 'asc') ? '-alt' : '';
                    echo Html::a('<span class="glyphicon glyphicon-sort-by-alphabet' . $alt .'"></span>',
                        ['/', 'order' => 'id', 'dir' => $alt]); ?>
                     ID
                </td>
                <td>
                    <?php
                    $alt = ($order === 'name' and $dir === 'asc') ? '-alt' : '';
                    echo Html::a('<span class="glyphicon glyphicon-sort-by-alphabet' . $alt .'"></span>',
                        ['/', 'order' => 'name', 'dir' => $alt]); ?>
                     Name
                </td>
                <td>Preview</td>
                <td>
                     Author
                </td>
                <td>
                    <?php
                    $alt = ($order === 'date' and $dir === 'asc') ? '-alt' : '';
                    echo Html::a('<span class="glyphicon glyphicon-sort-by-alphabet' . $alt .'"></span>',
                        ['/', 'order' => 'date', 'dir' => $alt]); ?>
                    Release date
                </td>
                <td>
                    <?php
                    $alt = ($order === 'date_create' and $dir === 'asc') ? '-alt' : '';
                    echo Html::a('<span class="glyphicon glyphicon-sort-by-alphabet' . $alt .'"></span>',
                        ['/', 'order' => 'date_create']); ?>
                    Date Added
                </td>
                <td>Actions</td>
            </tr>
            <?php foreach ($books as $book): ?>
                <tr>
                    <td>
                        <?php echo $book->id; ?>
                    </td>
                    <td><?php echo $book->name; ?></td>
                    <td>
                        <?php echo Html::a(Html::img($book->preview, ['class' => 'img-rounded',
                                'height' => '75px']),
                        $book->preview, ['class' => 'fancybox']); ?>
                    </td>
                    <td><?php echo $book->author->fullname; ?></td>
                    <td><?php echo date('d F Y', strtotime($book->date)); ?></td>
                    <td><?php echo date('m/d/Y H:i', $book->date_create); ?></td>
                    <td>
                        <?php echo Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                            ['books/edit', 'id'=>$book->id], ['class' => 'btn btn-success']); ?>
                        <?php echo Html::a('<span class="glyphicon glyphicon-play-circle"></span>',
                            ['#', 'id'=>$book->id], ['class' => 'btn btn-success show-modal', 'data-id' => $book->id]); ?>
                        <?php echo Html::a('<span class="glyphicon glyphicon-remove-circle"></span>',
                            ['books/delete', 'id'=>$book->id], ['class' => 'btn btn-danger']); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
</div>

<div class="modal fade" id="show-modal" tabindex="-1" role="dialog" aria-labelledby="show-modal_label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
