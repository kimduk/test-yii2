<?php

use yii\db\Schema;
use yii\db\Migration;

class m150529_075202_books extends Migration
{
    public function safeUp()
    {
        $this->createTable('books', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'date_create' => Schema::TYPE_INTEGER,
            'date_update' => Schema::TYPE_INTEGER,
            'preview' => Schema::TYPE_STRING,
            'date' => Schema::TYPE_DATE,
            'author_id' => Schema::TYPE_INTEGER,
        ]);

        $this->insert('books', [
            'name' => 'Web Application Development with Yii and PHP',
            'date_create' => time(),
            'date_update' => time(),
            'preview' => 'http://ecx.images-amazon.com/images/I/51om2Dpe93L.jpg',
            'date' => '2012.11.19',
            'author_id' => 1
        ]);

        $this->insert('books', [
            'name' => 'Yii Application Development Cookbook',
            'date_create' => time(),
            'date_update' => time(),
            'preview' => 'http://ecx.images-amazon.com/images/I/51wKCp3gr1L.jpg',
            'date' => '2013.04.24',
            'author_id' => 2
        ]);

        $this->insert('books', [
            'name' => 'THE YII BOOK',
            'date_create' => time(),
            'date_update' => time(),
            'preview' => 'https://larry.pub/images/yiibookcover.png',
            'date' => '2014.01.01',
            'author_id' => 3
        ]);

        $this->addForeignKey('authors_fk', 'books', 'author_id', 'authors', 'id');
    }

    public function down()
    {
        $this->dropTable('books');
    }
}
