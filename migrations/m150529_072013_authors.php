<?php

use yii\db\Schema;
use yii\db\Migration;

class m150529_072013_authors extends Migration
{
    public function safeUp()
    {
        $this->createTable('authors', [
            'id' => Schema::TYPE_PK,
            'firstname' => Schema::TYPE_STRING,
            'lastname' => Schema::TYPE_STRING
        ]);

        $this->insert('authors', [
            'firstname' => 'Jeffrey',
            'lastname' => 'Winesett'
        ]);

        $this->insert('authors', [
            'firstname' => 'Alexander',
            'lastname' => 'Makarov'
        ]);

        $this->insert('authors', [
            'firstname' => 'Larry',
            'lastname' => 'Ullman'
        ]);
    }

    public function down()
    {
        $this->dropTable('authors');
    }
}
