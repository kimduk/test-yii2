<?php

namespace app\controllers;

use app\models\Authors;
use app\models\Books;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;

class BooksController extends MainController
{
    public function actionShow()
    {
        $id = Yii::$app->getRequest()->getQueryParam('id');

        $book = Books::findOne((int)$id);

        $this->layout = false;
        return $this->render('show', [
            'book' => $book
        ]);
    }

    public function actionEdit()
    {
        $id = Yii::$app->getRequest()->getQueryParam('id');

        if ($id === NULL)
            throw new HttpException(404, 'Not Found');

        $bookModel = Books::findOne((int)$id);

        if ($bookModel === NULL)
            throw new HttpException(404, 'Document Does Not Exist');

        if ($bookModel->load(Yii::$app->request->post())) {
            $bookModel->imageFile = UploadedFile::getInstance($bookModel, 'imageFile');

            if ($bookModel->imageFile) {
                $bookModel->preview = 'uploads/' . $bookModel->imageFile->baseName . '.' . $bookModel->imageFile->extension;
            }

            if ($bookModel->save()) {
                if ($bookModel->imageFile) {
                    $bookModel->imageFile->saveAs('uploads/' . $bookModel->imageFile->baseName . '.' . $bookModel->imageFile->extension);
                }
                Yii::$app->session->setFlash('info', 'Book information updated.');
                return Yii::$app->response->redirect(['/']);
            } else {
                Yii::$app->session->setFlash('error', 'Information is not updated.');
            }
        }
        $authors = Authors::find()->all();

        return $this->render('edit', [
            'bookModel' => $bookModel,
            'authors' => $authors,
        ]);
    }
}
