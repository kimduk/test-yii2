<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 27.07.15
 * Time: 18:33
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;

class MainController extends Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($this->action->id !== 'login' AND $this->action->id !== 'logout') {
                if(Yii::$app->user->isGuest) $this->redirect('/site/login');
            }
            return true;
        } else {
            return false;
        }
    }
} 