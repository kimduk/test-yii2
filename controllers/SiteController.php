<?php

namespace app\controllers;

use app\models\Authors;
use app\models\Books;
use app\models\SearchForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\ArrayHelper;

class SiteController extends MainController
{

    private $_ordersList = [
        'id', 'fullname', 'name', 'date', 'date_create'
    ];

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $order = Yii::$app->getRequest()->getQueryParam('order');
        $order = (!empty($order) and in_array($order, $this->_ordersList)) ? $order : 'id';

        $dir = Yii::$app->getRequest()->getQueryParam('dir');
        $dir = (!empty($dir)) ? 'desc' : 'asc';

        $books = Books::find()->with('author')->orderBy($order . ' ' . $dir);
        $authors = Authors::find()->all();
        $searchForm = new SearchForm();

        if ($searchForm->load(Yii::$app->request->post()))
        {
            $data = Yii::$app->request->getBodyParam('SearchForm');

            $books->where(1);
            if (!empty($data['authorId']) and ((int)$data['authorId'] > 0))
            {
                $books->andWhere(['author_id' => (int)$data['authorId']]);
            }
            if (!empty($data['name']))
            {
                $books->andWhere(['like', 'name', $data['name']]);
            }
            if (empty($data['releaseDateFrom']))
            {
                $data['releaseDateFrom'] = '1900-01-01';
            }
            if (empty($data['releaseDateTo']))
            {
                $data['releaseDateTo'] = date('Y-m-d');
            }
            $books->andWhere(['between', 'date', $data['releaseDateFrom'], $data['releaseDateTo']]);
//var_dump($books->createCommand()->rawSql);die;
            $searchForm->name = $data['name'];
            $searchForm->authorId = (int)$data['authorId'];
            $searchForm->releaseDateFrom = $data['releaseDateFrom'];
            $searchForm->releaseDateTo = $data['releaseDateTo'];
        }

        return $this->render('index', [
            'books' => $books->all(),
            'authors' => $authors,
            'searchForm' => $searchForm,
            'order' => $order,
            'dir' => $dir
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
